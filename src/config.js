// const baseURL = 'http://192.168.31.180:3000';
// const baseCodeURL = 'http://192.168.31.180:80';

// const baseURL = 'http://192.168.31.220:3000';
// const baseCodeURL = 'http://192.168.31.220:80';

// const baseURL = 'http://52.42.99.98:3000';
// const baseCodeURL = 'http://52.42.99.98:80';

// const baseURL = 'http://192.068.31.178:3001';
// const baseCodeURL = 'http://192.168.31.178:80';

const type = document.location.protocol;
const baseURL = type + '//api.star.store';
const baseCodeURL = type + '//api.star.store';

export default {
    baseURL: baseURL,
    baseCodeURL: baseCodeURL,
    duration: 9000,
    showImag: baseURL + '/uploads/',
    defaultImg: 'logo.png'
}
