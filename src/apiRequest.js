import axios from "axios";
import qs from 'qs';
import url from './config.js';

function baseRequest(method, path, params, data, type) {
    method = method.toUpperCase() || 'GET';
    let paramsobj = { params: params };
    axios.defaults.baseURL = url.baseURL;
    if (method === 'POST') {
        return axios.post(path, qs.stringify(data));
    } else if (method === 'GET') {
        return axios.get(path, paramsobj);
    } else {
        return axios.delete(path, qs.stringify(data));
    };
}
export let baseul = url.baseURL;

export let loginRequest = function(data) {
    return baseRequest('POST', '/brand/login', '', data);
};

export let addNewUser = function(data) {
    return baseRequest('POST', '/brand/reg', '', data);
};

export let getVerification = function(params) {
    return baseRequest('GET', '/brand/auth', params, '');
};

export let createCompaigns = function(data) {
    return baseRequest('POST', '/brand/campaign', '', data);
};

export let getCompaigns = function(params) {
    return baseRequest('GET', '/brand/allcampaign', params, '');
};

export let getCompaignsById = function(params) {
    return baseRequest('GET', '/brand/campaignbyid/' + params.id, '', '');
};

export let getCompaignsData = function(params) {
    return baseRequest('GET', '/brand/stat-campaign', params, '');
};

export let settingUserMsg = function(data) {
    return baseRequest('POST', '/brand/company', '', data);
};

export let settingUserPassword = function(data) {
    return baseRequest('POST', '/brand/changepwd', '', data);
};

export let getPhoneUnic = function(params) {
    return baseRequest('GET', '/brand/phone/' + params, '', '');
};

export let changeCampaignState = function(data) {
    return baseRequest('POST', '/brand/change-campaign-state', '', data);
};

export let getPhoneCode = function(params) {
    return baseRequest('GET', '/services/sendcode', params, '');
};

export let getPhonecaptcha = function(params) {
    return baseRequest('GET', '/services/captcha', params, '');
};

export let updataBrandStatus = function(params) {
    return baseRequest('GET', '/operation/update_company_state', params, '');
};

export let getBrandUserMsg = function(params) {
    return baseRequest('GET', '/brand/get_brand_user', params, '');
};

export let getFindPassword = function(data) {
    return baseRequest('POST', '/brand/findpwd', '', data);
};
