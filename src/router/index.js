import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/Home';
import Page from '@/components/Page';
import Login from '@/components/user/Login';
import NewUser from '@/components/user/NewUser';
import ForgetPassWord from '@/components/user/ForgetPassWord';

import Campaings from '@/components/page/Campaings';
import CampaingsDetails from '@/components/page/CampaingsDetails';
import CreateCampings from '@/components/page/CreateCampings';
import Wallet from '@/components/page/Wallet';
import Settings from '@/components/page/Settings';
import Faq from '@/components/page/Faq';

Vue.use(Router);

export default new Router({
    routes: [{
            path: '/home',
            name: 'Hello',
            component: Home
        },
        {
            path: '/page',
            name: 'Page',
            component: Page,
            children: [
                { path: 'campaings', component: Campaings },
                { path: 'campaingsDetails/:id', component: CampaingsDetails },
                { path: 'createCampings', component: CreateCampings },
                { path: 'wallet', component: Wallet },
                { path: 'settings', component: Settings },
                { path: 'faq', component: Faq }
            ]
        },
        { path: '/login', component: Login },
        { path: '/newUser', component: NewUser },
        { path: '/', redirect: '/login' },
        { path: '/forgetPassWord', component: ForgetPassWord },
        { path: '*', component: Login }
    ]
})
