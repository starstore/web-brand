// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-default/index.css';
import 'font-awesome/css/font-awesome.min.css';
import './index.css';
import { getVerification } from './apiRequest';
import Promise from 'whatwg-fetch';
import 'babel-polyfill';
import locale from 'element-ui/lib/locale/lang/en';

Vue.use(ElementUI, { locale });

if (!window.Promise) {
    window.Promise = Promise;
}

Vue.config.productionTip = false;

router.beforeEach((to, from, next) => {
    if (to.path === '/login' || to.path === '/newUser' || to.path === '/page' || to.path === '/forgetPassWord') {
        return next();
    }
    // console.log('开始检测 token');
    getVerification({ token: sessionStorage.getItem('dataToken') }).then((data) => {
        // console.log(data);
        if (data.data.code === 0) {
            // console.log('验证通过', data.data);
            next();
        } else {
            // console.log('验证不通过，跳登陆页面');
            next({ path: '/login' });
        }
    }).catch((res) => {
        // console.log(res);
        next({ path: '/login' });
    });
})

new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: { App }
})